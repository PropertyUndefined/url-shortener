import express from "express";
import { addUrl } from "../src/controllers/redisController";
var router = express.Router();

/* GET home page. */
router.post("/", async function (req, res, next) {
  console.log(req.body);
  const url = await addUrl(req.body.url);
  res.send(url);
});

export default router;
