import axios from "axios";

const form = document.querySelector("form");
const input = document.querySelector("input");
const result = document.querySelector(".result");
form.addEventListener("submit", (event) => {
  event.preventDefault();
  handleSubmit();
});

const handleSubmit = async () => {
  const url = input.value;
  const reply = await axios.post("/url", {
    url: url,
  });
  result.innerHTML = `${window.location.href}${reply}`;
};
