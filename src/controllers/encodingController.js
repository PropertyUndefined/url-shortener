import base62 from "base62";

export const encode = (number) => base62.encode(number);

export const decode = (string) => base62.decode(string);

export const getUniqueId = (id) => encode(Date.now() + id);
