import redis from "redis";
import util from "util";
import { getUniqueId } from "./encodingController";

const client = redis.createClient();
const aSelect = util.promisify(client.select).bind(client);
const aSet = util.promisify(client.set).bind(client);
const aIncr = util.promisify(client.incr).bind(client);
const URL_TABLE = 0;
const ID_TABLE = 1;

export const addUrl = async (url) => {
  await aSelect(ID_TABLE);
  const id = await aIncr("current key");
  const encodedId = getUniqueId(id);
  await aSelect(URL_TABLE);
  await aSet(encodedId, url);
  return encodedId;
};
